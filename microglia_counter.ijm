//Microglia counting script v1 - B.M.Davis@ucl.ac.uk

function isImages(filename) {
	extensions = newArray("tif","tiff","jpg","bmp");
	result=false;
	for (i=0; i<extensions.length; i++) {
		if (endsWith(toLowerCase(filename),"."+extensions[ i]))
		result=true;
	}
		return result;
	}

function countImages(array) {
	result = 0;
	for (i=0; i<array.length; i++) {
		if (isImages(array[ i] ) ) result++;
	}
		return result;
	}


//Batch processing
inputFolder = getDirectory("Microglia counter - Where are your images?");
outputFolder = getDirectory("Microglia counter - Where should I send your results?");
setBatchMode(true);
images = getFileList(inputFolder);
imageCount = countImages(images);
//notice = "converted: \n";

for (i=0;i<images.length; i++) {
	showProgress(i, imageCount);
	inputPath = inputFolder + images[ i];
	if (isImages(inputPath)) {
		open(inputPath);
		
			//Clear memory
			call("java.lang.System.gc");

name = getTitle();			
//Step 1. Set the image scale to pixels and measurements for the protocol
run("Set Scale...", "distance=1 known=1");
run("Set Measurements...", "area centroid perimeter shape display redirect=None decimal=3");
run("Duplicate...", "title=GPU");
run("Duplicate...", "title=Spots");

run("8-bit");

run("Gray Scale Attribute Filtering", "operation=Opening attribute=Area minimum=25 connectivity=8");

run("Morphological Filters", "operation=Opening element=Octagon radius=1");

setAutoThreshold("MaxEntropy dark");
setOption("BlackBackground", true);
run("Convert to Mask");

run("Analyze Particles...", "size=10-100000 pixel display clear summarize add in_situ");
run("Nnd ");
	

//Save Results
			selectWindow("Results");
			saveAs("Measurements",""+outputFolder+name+"_results.txt");		
	}
}

//Save Stack Summary
selectWindow("Summary");
saveAs("Measurements",""+outputFolder+"_Results_Summary.txt");

//Finishing
setBatchMode(false);
//print(notice);
run("Close All");
exit();

//Microglia counting script v1 - B.M.Davis@ucl.ac.uk